// JavaScript Document
/* graph related */
			
function renderPieChart_ProposedAllocation(categories, series) {
    chartid = "renderPieChart_ProposedAllocation";
    databind = data_BalancesByAssets;
    title = "";
    ///////////////////////////////////////////////////////////
    var fn = arguments.callee;
    if (!categories) {
        categories = databind.categories;
    };
    if (!series) {
        series = databind.series;
    };
    fontfamily = "Arial";
    fontweight = "bold";
    fontsize_title = "7px";
    fontsize_labels = "4px";
    if ($.browser.msie) {
        fontsize_labels = "5px"
    };
    var chartP;
    chartP = new Highcharts.Chart({
        chart: {
            renderTo: chartid,           
            spacingTop: 0,
            spacingRight: 0,
            spacingBottom: 20,
            spacingLeft: 0,
			height: 180,
			width:450
        }, 		
		colors: ['#01A0AF', '#0153A3', '#668D1C', '#EBAE1E', '#803885', '#494B4E'],
		title: {
                text: ''
            },
		legend: {
		   layout: 'vertical',
           verticalAlign: 'right',		   
            x: 5,
            y: 20,
			 borderWidth:0,
		   borderRadius: 0
		},
		credits: {
            enabled: false
        },
        tooltip: {},
        plotOptions: {
             pie: {
					size:'75%',
                    allowPointSelect: true,
                    cursor: 'pointer',
					center: [100, 100],
                    dataLabels: {
                        enabled: false
                    },
					showInLegend: true
                }
        }, 
        series: series
    }); };
	
	var data_BalancesByAssets = {
    series: [{
                type: 'pie',
                name: '',
                data: [
				    ['Guaranteed',   13],
                    ['Equities',   54],
					['Fixed Income',   24.1],					
                    ['Real Estate',   8.1],
                    ['Money Market',   0.8],				
					['Not Assigned',   0.2]
                ]
            }] };
			
			
function renderPieChart_AssetAllocation(categories, series) {
    chartid = "renderPieChart_AssetAllocation";
    databind = data_ContributionsByAsset;
    title = "";
    ///////////////////////////////////////////////////////////
    var fn = arguments.callee;
    if (!categories) {
        categories = databind.categories;
    };
    if (!series) {
        series = databind.series;
    };
    fontfamily = "Arial";
    fontweight = "bold";
    fontsize_title = "7px";
    fontsize_labels = "4px";
    if ($.browser.msie) {
        fontsize_labels = "5px"
    };
    var chartP;
    chartP = new Highcharts.Chart({
        chart: {
            renderTo: chartid,           
            spacingTop: 0,
            spacingRight: 0,
            spacingBottom: 20,
            spacingLeft: 0,
			height: 180,
			width:450
        }, 		
			colors: ['#01A0AF', '#0153A3', '#668D1C', '#EBAE1E', '#803885', '#494B4E'],
		title: {
                text: ''
            },
		legend: {
		   layout: 'vertical',
           verticalAlign: 'right',		   
            x: 5,
            y: 20,
			 borderWidth:0,
		   borderRadius: 0
		},
		credits: {
            enabled: false
        },
        tooltip: {},
        plotOptions: {
             pie: {
					size:'75%',
                    allowPointSelect: true,
                    cursor: 'pointer',
					center: [100, 100],
                    dataLabels: {
                        enabled: false
                    },
					showInLegend: true
                }
        }, 
        series: series
    }); };
	
	var data_ContributionsByAsset = {
    series: [{
                type: 'pie',
                name: '',
                data: [
				
				    ['Guaranteed',   1.1],
                    ['Equities',   27.8],
					['Fixed Income',   55.1],					
                    ['Real Estate',   0.5],
                    ['Money Market',   15.5]				
				
                ]
            }] };
			
	function renderPieChart_RetAssetAllocation(categories, series) {
    chartid = "renderPieChart_RetAssetAllocation";
    databind = data_ContributionsByAsset;
    title = "";
    ///////////////////////////////////////////////////////////
    var fn = arguments.callee;
    if (!categories) {
        categories = databind.categories;
    };
    if (!series) {
        series = databind.series;
    };
    fontfamily = "Arial";
    fontweight = "bold";
    fontsize_title = "7px";
    fontsize_labels = "4px";
    if ($.browser.msie) {
        fontsize_labels = "5px"
    };
    var chartP;
    chartP = new Highcharts.Chart({
        chart: {
            renderTo: chartid,           
            spacingTop: 0,
            spacingRight: 0,
            spacingBottom: 20,
            spacingLeft: 0,
			height: 180,
			width:450
        }, 		
		colors: ['#0153A3', '#803885', '#668D1C', '#522E6E', '#01A0AF', '#EBAE1E', '#494B4E'],
		title: {
                text: ''
            },
		legend: {
		   layout: 'vertical',
           verticalAlign: 'right',		   
            x: 5,
            y: 20,
			 borderWidth:0,
		   borderRadius: 0
		},
		credits: {
            enabled: false
        },
        tooltip: {},
        plotOptions: {
             pie: {
					size:'75%',
                    allowPointSelect: true,
                    cursor: 'pointer',
					center: [100, 100],
                    dataLabels: {
                        enabled: false
                    },
					showInLegend: true
                }
        }, 
        series: series
    }); };
	
	var data_ContributionsByAsset = {
    series: [{
                type: 'pie',
                name: '',
                data: [
                    ['Equities',   15],
                    ['Money Market',   4],
					['Fixed Income',   30],
                    ['Multi-Asset',   5],
					['Guaranteed',   2],
                    ['Real Estate',   3],
					['Not Assigned',   5]
                ]
            }] };		
/*function renderPieChart_RetAssetAllocation(categories, series) {
    chartid = "renderPieChart_AssetAllocation";
    databind = data_ContributionsByAsset;
    title = "";
    ///////////////////////////////////////////////////////////
    var fn = arguments.callee;
    if (!categories) {
        categories = databind.categories;
    };
    if (!series) {
        series = databind.series;
    };
    fontfamily = "Arial";
    fontweight = "bold";
    fontsize_title = "7px";
    fontsize_labels = "4px";
    if ($.browser.msie) {
        fontsize_labels = "5px"
    };
    var chartP;
    chartP = new Highcharts.Chart({
        chart: {
            renderTo: chartid,           
            spacingTop: 0,
            spacingRight: 0,
            spacingBottom: 20,
            spacingLeft: 0,
			height: 180,
			width:450
        }, 		
			colors: [ '#0153A3', '#494B4E'  '#494B4E' '#668D1C', '#EBAE1E', '#01A0AF', '#803885', ],
		title: {
                text: ''
            },
		legend: {
		   layout: 'vertical',
           verticalAlign: 'right',		   
            x: 5,
            y: 20,
			 borderWidth:0,
		   borderRadius: 0
		},
		credits: {
            enabled: false
        },
        tooltip: {},
        plotOptions: {
             pie: {
					size:'75%',
                    allowPointSelect: true,
                    cursor: 'pointer',
					center: [100, 100],
                    dataLabels: {
                        enabled: false
                    },
					showInLegend: true
                }
        }, 
        series: series
    }); };
	
	var data_ContributionsByAsset = {
    series: [{
                type: 'pie',
                name: '',
                data: [
				
				    
                    ['Equities',   15],
					['Money Market',   4]
					['Fixed Income',   30],	
                    ['Multi-Asset',   5],
                     ['Guaranteed',   2],					
                    ['Real Estate',   3],
					['Not Assigned',   5]
                    				
				
                ]
            }] };*/
			
			
				var data_AssetAllocation = {
    series: [{
                type: 'pie',
                name: '',
                data: [
				
				    ['Guaranteed',   1.1],
                    ['Equities',   27.8],
					['Real Estate',   0.5],
					['Fixed Income',   55.1],
                    ['Money Market',   15.5]				
				
                ]
            }] };
			
	function renderPieChart_AssetAllocation(categories, series) {
    chartid = "renderPieChart_AssetAllocation";
    databind = data_AssetAllocation;
    title = "";
    ///////////////////////////////////////////////////////////
    var fn = arguments.callee;
    if (!categories) {
        categories = databind.categories;
    };
    if (!series) {
        series = databind.series;
    };
    fontfamily = "Arial";
    fontweight = "bold";
    fontsize_title = "7px";
    fontsize_labels = "4px";
    if ($.browser.msie) {
        fontsize_labels = "5px"
    };
    var chartP;
    chartP = new Highcharts.Chart({
        chart: {
            renderTo: chartid,           
            spacingTop: 0,
            spacingRight: 0,
            spacingBottom: 20,
            spacingLeft: -20,
			height: 260,                                                                                                                                                                                                        
			width:130
        }, 		
		colors: ['#00A7B1','#0053A4', '#EBAE1D', '#668F1B', '#7F3785' ],
		title: {
                text: ''
            },
		legend: {
		   enabled: false
		},
		credits: {
            enabled: false
        },
        tooltip: {},
        plotOptions: {
             pie: {
					size:'75%',
                    allowPointSelect: true,
                    cursor: 'pointer',
					center: [80, 100],
                    dataLabels: {
                        enabled: false
                    },
					showInLegend: true
                }
        }, 
        series: series
    }); };
	
	
	var data_PAssetAllocation = {
    series: [{
                type: 'pie',
                name: '',
                data: [
				
				    ['Guaranteed',   1.1],
                    ['Equities',   27.8],
					['Real Estate',   0.5],
					['Fixed Income',   55.1],
                    ['Money Market',   15.5]				
				
                ]
            }] };
			
	function renderPieChart_PAssetAllocation(categories, series) {
    chartid = "renderPieChart_PAssetAllocation";
    databind = data_PAssetAllocation;
    title = "";
    ///////////////////////////////////////////////////////////
    var fn = arguments.callee;
    if (!categories) {
        categories = databind.categories;
    };
    if (!series) {
        series = databind.series;
    };
    fontfamily = "Arial";
    fontweight = "bold";
    fontsize_title = "7px";
    fontsize_labels = "4px";
    if ($.browser.msie) {
        fontsize_labels = "5px"
    };
    var chartP;
    chartP = new Highcharts.Chart({
        chart: {
            renderTo: chartid,           
            spacingTop: 0,
            spacingRight: 0,
            spacingBottom: 20,
            spacingLeft: -20,
			height: 260,                                                                                                                                                                                                        
			width:130
        }, 		
		colors: ['#00A7B1','#0053A4', '#EBAE1D', '#668F1B', '#7F3785' ],
		title: {
                text: ''
            },
		legend: {
		   enabled: false
		},
		credits: {
            enabled: false
        },
        tooltip: {},
        plotOptions: {
             pie: {
					size:'75%',
                    allowPointSelect: true,
                    cursor: 'pointer',
					center: [80, 100],
                    dataLabels: {
                        enabled: false
                    },
					showInLegend: true
                }
        }, 
        series: series
    }); };
	
	
	
	
			
			
